"""
Definition of views.
"""

from datetime import datetime
from django.shortcuts import render
from django.http import HttpRequest
from django.contrib.auth import get_user_model
from django_dtview import views as dtViews
from django_dtview.models import (dtview_dt, dtview_dtcol,
                                  dtview_dv, dtview_dvcol, dtview_cfg)
from django.http import JsonResponse
import json


def index(request):
    return render(request,
                  "test.html",
                  {
                  })


class ZobrazeniProDtview(dtViews.Dtview):
    def get_queryset(self, model, filters, request):
        return super().get_queryset(model, filters, request)
