var DtView = function (options) {
    var vars = {
        dtview_name: "",
        showPreFilter: 1,
        elementId: "",
        superiorFilter: "",
        checkboxCaseSensitive: 1,
        showSettings: 1,
        ShowFilteredData: "",
        constraints: {},
        dateFormat: "DD.MM.YYYY"
    };

    var root = this;
    this.construct = function (options) {
        $.extend(vars, options);
        validate.extend(validate.validators.datetime,
            {
                parse: function (value, options) {
                    return +moment.utc(value, dateFormat);
                },
                format: function (value, options) {
                    var format = dateFormat;
                    return moment.utc(value).format(format);
                }
            });
        Inicializace();
    };
    var Inicializace = function () {
        console.log(vars);
        var myDialog = document.getElementsByTagName('body')[0];
        let elementString;

        if (vars.showSettings == 1) {
            elementString =
                '<div class="modal fade" id="NastaveniModal" tabindex="-1" role="dialog" aria-hidden="true">\
                          <div class="modal-dialog" role="document" style="width:40vw">\
                            <div class="modal-content">\
                             <div class="modal-header">\
                                <button type = "button" class= "close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button >\
                                <h4 class="modal-title">Nastavení zobrazení</h4>\
                                </div>\
                              <div class="modal-body">\
                              <button id="dtViewFiltry" class="btn btn-secondary">Filtry</button>\
                                        <input type="hidden" name="operace" value=""/>\
                                        <div class="container" style="margin-bottom:8px">\
                                            <div class="form-inline">\
                                                <div class="form-group">\
                                                <label for="dtNazevZobrazeni">Název zobrazení</label>\
                                                        <input id="dtNazevZobrazeni" name="nazevZobrazeni" class="form-control" type="text">\
                                            </div>\
                                            </div>\
                                        </div>\
                                <div class="container-fluid">\
                                    <div class="row">\
                                 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">\
                                    <h3 style="text-align:left;float:left;">Zobrazované sloupce</h3><p style="text-align:right;float:right;">(přetáhněte mezi sloupci)</p>\
                                      <ul id="dtVybraneRadky" draggable="false" style="padding:12px;border-style: solid;border-width: 2px;border-color:green; clear:both;">\
                                        </ul>\
                                        </div>\
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>\
                                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">\
                                            <h3>Nezobrazované sloupce</h3><p style="text-align:right;float:right;">(přetáhněte mezi sloupci)</p>\
                                      <ul id="dtZbyleRadky" draggable="false" style="padding:12px;border-style: solid;border-width: 2px;border-color:black; clear:both;">\
                                    </ul>\
                                    </div>\
                                 </div>\
                                </div>\
                                 <div class="row form-check" id="dtNastaveniVerejne">\
                                <input id="dtNastaveniVerejneCheckBox" type="checkbox" name="verejne" style="margin-left:20px"> <label class="font-weight-bold" for="dtNastaveniVerejneCheckBox" >Veřejné zobrazení(uvidí ho všichni uživatelé)</label>\
                                 </div>\
                                </div>\
                                <div class="modal-footer">\
                <button id="dtBtnVymazatZobrazeni" style="float:right" class="btn btn-danger">Vymazat zobrazení</button>\
                <button id="dtBtnUlozitZobrazeni" style="float:right; margin-left:5px;margin-right:5px" class="btn btn-primary">Uložit jako nové zobrazení</button>\
                <button id="dtBtnUlozitZmeny" style="float:right;" class="btn btn-secondary">Uložit změny</button>\
                                </div>\
                                </div>\
                            </div>\
                          </div>\
                <div class="modal fade" id="FiltrModal" tabindex="-1" role="dialog" aria-hidden="true">\
                          <div class="modal-dialog " role="document" style="width:30vw">\
                            <div class="modal-content">\
                              <div class="modal-header">\
                                <button type = "button" class= "close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button >\
                                <h4 class="modal-title">Filtry</h4>\
                              </div>\
                              <div id="dtFiltrModalBody" class="modal-body">\
                <form name="datatableFiltr" id="datatableFiltr" novalidate></form>\
                        </div>\
                        <div class="modal-footer">\
                        </div>\
                        </div>\
                              </div>\
                            </div>\
                            <div class="modal fade" id="dvSkupinaFiltruModal" tabindex="-1" role="dialog" aria-hidden="true">\
                          <div class="modal-dialog " role="document" style="width:30vw">\
                            <div class="modal-content">\
                              <div class="modal-header">\
                                <h4 class="modal-title">Filtry zobrazení</h4>\
                              </div>\
                              <div id="dtFiltrModalBody" class="modal-body">\
                              <button id="dvPridatSkupinu" class="btn btn-primary">Přidat filtr</button>\
                <form name="dvFiltr" id="dvFiltr" novalidate></form>\
                        </div>\
                        <div class="modal-footer">\
                <button id="btnFiltryZrusit" style="float:right" class="btn btn-danger">Zrušit</button>\
                <button id="btnFiltryPridat" style="float:right; margin-left:5px;margin-right:5px" class="btn btn-primary">Uložit</button>\
                        </div>\
                        </div>\
                        </div>\
                              </div>\
                            </div>\
                            <div class="modal fade" id="dvJednotlivyFiltrModal" tabindex="-1" role="dialog" aria-hidden="true">\
                          <div class="modal-dialog " role="document" style="width:30vw">\
                            <div class="modal-content">\
                              <div class="modal-header">\
                                <h4 class="modal-title">Skupina filtrů</h4>\
                              </div>\
                              <div id="dtJednotlivyFiltrModalBody" class="modal-body">\
                              <button id="dvPridatFiltr" class="btn btn-primary">Přidat filtr</button>\
                <form name="dvJednotlivyFiltr" id="dvJednotlivyFiltr" novalidate></form>\
                        </div>\
                        <div class="modal-footer">\
                <button id="btnFiltryZrusit2" style="float:right" class="btn btn-danger">Zrušit</button>\
                <button id="btnFiltryPridat2" style="float:right; margin-left:5px;margin-right:5px" class="btn btn-primary">Uložit</button>\
                        </div>\
                        </div>\
                        </div>\
                              </div>\
                            </div>';
        } else {
            elementString =
                '<div class="modal fade" id="FiltrModal" tabindex="-1" role="dialog" aria-hidden="true">\
                          <div class="modal-dialog " role="document" style="width:30vw">\
                            <div class="modal-content">\
                              <div class="modal-header">\
                                <button type = "button" class= "close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button >\
                                <h4 class="modal-title">Filtry</h4>\
                              </div>\
                              <div id="dtFiltrModalBody" class="modal-body">\
                <form name="datatableFiltr" id="datatableFiltr" novalidate></form>\
                        </div>\
                        <div class="modal-footer">\
                        </div>\
                        </div>\
                              </div>\
                            </div>';
        }
        myDialog.insertAdjacentHTML('beforeend', elementString); //Ajax pro config
        var filtrUrl = normalUrl;
        var dataUrl = normalUrl;
        if(vars.superiorFilter && vars.superiorFilter.length > 0)
        {
            filtrUrl = vars.superiorFilter;
            dataUrl = vars.superiorFilter;
        }

        if (typeof vars.constraints === 'undefined') {
            vars.constraints = {};
        }

        jQuery.ajax({
            'async': true,
            'type': 'POST',
            'global': false,
            'url': dtview_urlConfig,
            'dataType': "json",
            beforeSend: function beforeSend(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            data: {
                'datatable': vars.dtview_name
            },
            'success': function success(JsonDatatableData) {
                if (typeof Object.assign != 'function') {
                    Object.assign = function (target) {
                        'use strict';
                        if (target == null) {
                            throw new TypeError('Cannot convert undefined or null to object');
                        }

                        target = Object(target);
                        for (var index = 1; index < arguments.length; index++) {
                            var source = arguments[index];
                            if (source != null) {
                                for (var key in source) {
                                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                                        target[key] = source[key];
                                    }
                                }
                            }
                        }
                        return target;
                    };
                }
                if (!String.prototype.endsWith) {
                    String.prototype.endsWith = function (search, this_len) {
                        if (this_len === undefined || this_len > this.length) {
                            this_len = this.length;
                        }
                        return this.substring(this_len - search.length, this_len) === search;
                    };
                }


                var json = JSON.parse(JsonDatatableData);
                var zobrazovatFiltry = vars.showPreFilter;
                var vybrany_table = json.dtview_dt.id; //id vybráného nastavení
                var filtrData = [];
                var filtrColkey = [];
                var filtrName = [];
                var optionsPole = [];
                var vybrane_zobrazeni;
                var dvSkupinyFiltru = [];
                var dvSkupinyFiltru_pridat = [];
                var vybranaSkupina = 0;
                var optionsOperators =
                    [
                        {
                            "text": "=",
                            "value": "=",
                            "selected": true
                        },
                        {
                            "text": "<",
                            "value": "<",
                        },
                        {
                            "text": ">",
                            "value": ">"
                        },
                        {
                            "text": "<=",
                            "value": "<=",
                        },
                        {
                            "text": ">=",
                            "value": ">=",
                        },
                        {
                            "text": "Obsahuje",
                            "value": "*=",
                        },
                        {
                            "text": "Začíná",
                            "value": "*<",
                        },
                        {
                            "text": "Končí",
                            "value": "*>",
                        },];

                if (json.dtview_cfg.dv_id != null) {
                    vybrane_zobrazeni = json.dtview_cfg.dv_id;
                } else {
                    vybrane_zobrazeni = json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.vychozi == 1;
                    })[0].id;
                } //získá všechny dostupné sloupce


                var vsechny_sloupce = [];
                json.dtview_dt.dtview_dtcol.filter(function (polozka) {
                    return polozka.visible == 1;
                }).forEach(function (item, index) {
                    vsechny_sloupce[index] = item;
                });
                vsechny_sloupce.sort(function (a, b) {
                    return a.poradi - b.poradi;
                }); //získá vybrané sloupce ze zobrazení

                var vybrane_sloupce = [];


                function nastav_vybrane_sloupce() {
                    vybrane_sloupce = [];
                    json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].dtview_dvcol.forEach(function (item, index) {
                        vybrane_sloupce[index] = item;
                    }); //Vezme sloupce ze zobrazeni

                    vybrane_sloupce.sort(function (a, b) {
                        return a.poradi - b.poradi;
                    });
                    vybrane_sloupce = vybrane_sloupce.map(function (e) {
                        return vsechny_sloupce.filter(function (a) {
                            return a.id == e.col_id && a.visible == 1;
                        })[0];
                    });
                    vybrane_sloupce = vybrane_sloupce.filter(function (sloupec) {
                        return sloupec != null;
                    });
                    optionsPole = [];
                    vybrane_sloupce.forEach(function (sloupec) {
                        var objekt = {
                            text: sloupec.label,
                            value: sloupec.colkey
                        };
                        optionsPole.push(objekt);
                    })
                }

                nastav_vybrane_sloupce(); //ziska dostupne zobrazeni

                var vsechny_zobrazeni = [];
                json.dtview_dv.filter(function (zobrazeni) {
                    return zobrazeni.user_id == json.dtview_cfg.user_id || zobrazeni.verejne == 1;
                }).forEach(function (item, index) {
                    vsechny_zobrazeni[index] = item;
                }); //data format a title pro datatable

                var data_colkey;
                var data_label;
                var columnKeys;

                function nastav_collumkey() {
                    data_colkey = vybrane_sloupce.map(function (sloupec) {
                        return sloupec.colkey;
                    });
                    data_label = vybrane_sloupce.map(function (sloupec) {
                        return sloupec.label;
                    });
                    columnKeys = [];
                    vybrane_sloupce.forEach(function (item, index) {
                        columnKeys[index] = {
                            data: data_colkey[index],
                            title: data_label[index],
                            render: null
                        };
                    });
                }

                nastav_collumkey();
                var tabulka;



                if (vars.showPreFilter == 1) {
                    window.dtFiltrovat = function dtFiltrovat() {
                        document.getElementById(vars.elementId).style.display = "none";
                        zobrazovatFiltry = false;
                        var splnuje = false;
                        jQuery("#predchazejiciFiltr").children().find("input[type='text']").each(function (index) {
                            if (!splnuje && (jQuery(this).val() !== "" || jQuery(this).val()[0] !== undefined)) {
                                splnuje = true;
                            }
                        });
                        if (splnuje) {
                            inicializace_tabulky("formular");
                        } else {
                            inicializace_tabulky("nacteni");
                        }
                    };
                    //Zobrazí filtrovací formulář
                    var element = document.getElementById(vars.elementId);
                    element.insertAdjacentHTML("beforeend", "<form name='predchazejiciFiltr' id='predchazejiciFiltr'></form>");
                    element = document.getElementById('predchazejiciFiltr');
                    vybrane_sloupce.filter(function (sloupec) {
                        return sloupec.filterable == 1;
                    }).forEach(function (item, index) {
                        if (item.typ == "date") {
                            if (item.konverze == "dateonly") {
                                element.insertAdjacentHTML("beforeend", '<div class="form-group row vyhledavani-' + item.colkey + '" style="margin-bottom:8px"><label class="col-sm-2 col-form-label" for="Filtry_item' + item.colkey + '">' + item.label + ':</label><div class="col-sm-5"><input id="Filtry_item' + item.colkey + '" type="text" autocomplete="off" placeholder="' + item.label + '" name="' + item.colkey + '" class="dtviewFilter-dateonly form-control" /></div><div class="col-sm-5 messages"></div ></div>');
                                jQuery(document.forms["predchazejiciFiltr"]).children().children().children('input[name="' + item.colkey + '"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["predchazejiciFiltr"], vars.constraints) || {};
                                        zobrazitErrory(jQuery(document.forms["predchazejiciFiltr"][item.colkey])[0], errors[jQuery(document.forms["predchazejiciFiltr"][item.colkey]).attr("name")]);
                                    }
                                });

                            } else {
                                element.insertAdjacentHTML("beforeend", '<div class="form-group vyhledavani-' + item.colkey + '"><div class="form-group row"><label class="col-sm-2 col-form-label" for="Filtry_item' + item.colkey + '_od">' + item.label + ':</label><div class="col-sm-5"><input type="text" autocomplete="off" class="dtviewFilter-date form-control" name="' + item.colkey + '_od" placeholder = "' + item.label + '(od)")"' + '"/></div><div class="messages col-sm-4"></div></div> <div class="form-group row"><div class="col-sm-2"></div><div class="col-sm-5"><input type="text" class="dtviewFilter-date form-control" autocomplete="off"  name="' + item.colkey + '_do" placeholder = "' + item.label + '(do)" Formát: (dd.mm.YYYY nebo dd.mm)"' + '"/></div><div class="messages col-sm-4"></div></div></div>');

                                jQuery(document.forms["predchazejiciFiltr"]).children().children().children().children('input[name="' + item.colkey + '_od"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onShow: function (ct) {
                                        this.setOptions({
                                            maxDate: jQuery('#predchazejiciFiltr').children().children().children().children("input[name=" + item.colkey + "_do]").val() ? jQuery('#predchazejiciFiltr').children().children().children().children("input[name=" + item.colkey + "_do]").val() : false,
                                            formatDate: 'd.m.Y',
                                        })
                                    },
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["predchazejiciFiltr"], vars.constraints) || {};
                                        zobrazitErrory(jQuery(document.forms["predchazejiciFiltr"][item.colkey + '_od'])[0], errors[jQuery(document.forms["predchazejiciFiltr"][item.colkey + '_od']).attr("name")]);
                                    }
                                });
                                jQuery(document.forms["predchazejiciFiltr"]).children().children().children().children('input[name="' + item.colkey + '_do"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onShow: function (ct) {
                                        this.setOptions({

                                            minDate: jQuery('#predchazejiciFiltr').children().children().children().children("input[name=" + item.colkey + "_od]").val() ? jQuery('#predchazejiciFiltr').children().children().children().children("input[name=" + item.colkey + "_od]").val() : false,
                                            formatDate: 'd.m.Y',
                                        })
                                    },
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["predchazejiciFiltr"], vars.constraints) || {};
                                        zobrazitErrory(jQuery(document.forms["predchazejiciFiltr"][item.colkey + '_do'])[0], errors[jQuery(document.forms["predchazejiciFiltr"][item.colkey + '_do']).attr("name")]);
                                    }
                                });
                            }


                        } else {
                            element.insertAdjacentHTML("beforeend", '<div class="form-group row vyhledavani-' + item.colkey + '" style="margin-bottom:8px"><label class="col-sm-2 col-form-label" for="Filtry_item' + item.colkey + '">' + item.label + ':</label><div class="col-sm-5"><input id="Filtry_item' + item.colkey + '" type="text" placeholder="' + item.label + '" name="' + item.colkey + '" class="form-control" /></div><div class="col-sm-5 messages"></div ></div>');
                        }
                    });
                    if (vars.checkboxCaseSensitive == 1) {
                        element.insertAdjacentHTML("beforeend",
                            "<div class='float-right'><label class='form-check-label' for='FiltrdtNeIgnorovatVelka'> Rozlišovat velká a malá písmena</label><input type='checkbox' class='ml-2' style='margin-right: 8px' id='FiltrdtNeIgnorovatVelka' value='neignorovat'/><button type='submit' id='dtFiltrFiltrovat' class='btn btn-primary mr-2'>Pokračovat</button></div>");
                    } else {
                        element.insertAdjacentHTML("beforeend", "<div class='float-right'><label class='form-check-label' for='FiltrdtNeIgnorovatVelka' style='display:none'> Rozlišovat velká a malá písmena</label><input type='checkbox' class='ml-2' style='margin-right: 8px;display:none' id='FiltrdtNeIgnorovatVelka' value='neignorovat'/><button type='submit' id='dtFiltrFiltrovat' class='btn btn-primary mr-2'>Pokračovat</button></div>");
                    }
                    jQuery.datetimepicker.setLocale('cs');
                    var inputs = document.forms["predchazejiciFiltr"].querySelectorAll("input[type='text']", "textarea", "select");
                    for (let i = 0; i < inputs.length; ++i) {
                        inputs.item(i).addEventListener("change",
                            function () {

                                var errors = validate(document.forms["predchazejiciFiltr"], vars.constraints) || {};
                                zobrazitErrory(this, errors[this.name]);
                            });
                    }


                } else {
                    inicializace_tabulky("nacteni");
                    aktualizujFiltry();
                } //inicializuje Datatable


                var form1 = document.getElementById("predchazejiciFiltr");
                if (form1) {
                    form1.addEventListener("submit", function (ev) {
                        ev.preventDefault();
                        handleFormSubmit(form1);
                    });
                }

                var form2 = document.getElementById("datatableFiltr");
                form2.addEventListener("submit", function (ev) {
                    ev.preventDefault();
                    handleFormSubmit(form2);
                });


                function handleFormSubmit(form) {
                    var errors = validate(form, vars.constraints);
                    showErrors(form, errors || {});
                    if (!errors) {
                        if (form == document.getElementById("datatableFiltr")) {
                            let values = validate.collectFormValues(form);
                            for (var propName in values) {
                                if (values[propName] === null || values[propName] === undefined) {
                                    delete values[propName];
                                }
                            }
                            if (Object.keys(values).length > 0) {
                                window.dtBtnFiltrOnClick();
                            } else {
                                alert("Alespoň 1 pole musí být vyplněno");
                            }

                        } else {
                            window.dtFiltrovat();
                        }
                    }
                }

                function showErrors(form, errors) {
                    jQuery(form.querySelectorAll("input[name], select[name]")).each(function (index, input) {
                        zobrazitErrory(input, errors && errors[input.name]);
                    });
                }

                function zobrazitErrory(input, errors) {
                    var formGroup = closestParent(input.parentNode, "form-group");
                    messages = formGroup.querySelector(".messages");
                    resetFormGroup(formGroup);
                    if (errors) {
                        formGroup.classList.add("has-error");
                        errors.forEach(function (error) {
                            addError(messages, error);
                        });
                    } else {
                        formGroup.classList.add("has-success");
                    }
                }

                function addError(messages, error) {
                    var block = document.createElement("p");
                    block.classList.add("help-block");
                    block.classList.add("error");
                    block.innerText = error;
                    messages.appendChild(block);
                }

                function closestParent(child, className) {
                    if (!child || child == document) {
                        return null;
                    }
                    if (child.classList.contains(className)) {
                        return child;
                    } else {
                        return closestParent(child.parentNode, className);
                    }
                }

                function resetFormGroup(formGroup) {
                    // Remove the success and error classes
                    formGroup.classList.remove("has-error");
                    formGroup.classList.remove("has-success");
                    // and remove any old messages
                    jQuery(formGroup.querySelector(".help-block.error")).each(function (x, el) {
                        el.parentNode.removeChild(el);
                    });
                }


                var detailRows = [];


                function dictionaryUnique(array, array2) {
                    var result = [];
                    for (let i = 0; i < array.length; ++i) {
                        for (let j = 0; j < array2.length; ++i) {
                            if (array[i] !== array2[j]) {
                                result.append(array2[j])
                            }
                        }
                    }
                }

                function arrayUnique(array) {
                    var a = array.concat();
                    for (var i = 0; i < a.length; ++i) {
                        for (var j = i + 1; j < a.length; ++j) {
                            if (a[i] === a[j])
                                a.splice(j--, 1);
                        }
                    }

                    return a;
                }
                if(vars.showSettings == 1) {
                var strDvFiltr = json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].filtry;
                    strDvFiltr.split(";").forEach(function (_skupina) {
                        var skupina = []
                        _skupina.split(",").forEach(function (filtr) {
                            var objekt;
                            if (filtr.indexOf("<=")!=-1) {
                                objekt = filtr.split(/(<=)/g);
                            }
                            else if(filtr.indexOf(">=")!=-1)
                            {
                                objekt = filtr.split(/(>=)/g);
                            }
                            else if(filtr.indexOf("*<")!=-1)
                            {
                                objekt = filtr.split(/(\*<)/g);
                            }
                            else if(filtr.indexOf("*>")!=-1)
                            {
                                objekt = filtr.split(/(\*>)/g);
                            }
                            else if(filtr.indexOf("*=")!=-1)
                            {
                                objekt = filtr.split(/(\*=)/g);
                            }
                            else
                            {
                                objekt = filtr.split(/([=|<|>])/g);
                            }
                            console.log(objekt);
                            if(objekt.length>1)
                            {
                                skupina.push({"sloupec":objekt[0], "operace":objekt[1], "hodnota":objekt[2]});
                            }

                        })

                        if(skupina.length>0)
                        {
                            dvSkupinyFiltru.push(skupina);
                        }
                    })
                }
                function inicializace_tabulky(zdroj) {
                    dtPridatTlacitka();
                    let ajaxToUse = {};
                    let namePredchozi = "";
                    let index = 0;
                    switch (zdroj) {
                        case "filtr"://refresh přišel z filtrovani
                            var splnuje = false;
                            var arrayInput = [,]; //Seznam zadaných hodnot (Michal)

                            var arrayInputPlaceholder = []; //Colkeys
                            namePredchozi = "";
                            index = 0;
                            jQuery('#datatableFiltr').children().children('input[type="text"]').each(function () {
                                if (this.classList.contains("dtviewFilter-date")) {

                                    if (namePredchozi != jQuery(this).attr('name').substr(0, jQuery(this).attr('name').length - 3)) {
                                        arrayInputPlaceholder[index] = jQuery(this).attr('name');
                                        arrayInput[index] = [jQuery(this).val()];
                                        namePredchozi = jQuery(this).attr('name').substr(0, jQuery(this).attr('name').length - 3);
                                    } else {
                                        index--;
                                        arrayInput[index].push(jQuery(this).val());
                                    }
                                } else {
                                    arrayInputPlaceholder[index] = jQuery(this).attr('name');
                                    arrayInput[index] = jQuery(this).val();
                                }
                                if (!splnuje && jQuery(this).val().length > 0) {
                                    splnuje = true;
                                }
                                index++;
                            });
                            if (splnuje) {
                                let array = [];
                                array = arrayInputPlaceholder.map(function (el) {
                                    if (el.endsWith("_od") || el.endsWith("_do")) {
                                        return el.substr(0, el.length - 3);
                                    }
                                    return el;
                                }); //Seznam parametrů zadaných hodnot (jmeno)

                                if (zobrazovatFiltry) {
                                    document.getElementById("dtBtnZrusitFiltr2").style.display = "inline";
                                } //pro kazdy zaznam zkontroluje jestli plni všechny zadané podmínky
                                x = arrayInput.length;
                                for (var i = 0; i < x; ++i) {
                                    if (arrayInput[i].length <= 0 | (arrayInput[i][0] == "" && arrayInput[i][1] == "")) {
                                        arrayInput.splice(i, 1);
                                        array.splice(i, 1);
                                        arrayInputPlaceholder.splice(i, 1);
                                        --i;
                                        --x;
                                    }
                                }

                                arrayInput = arrayInput.filter(function (el) {
                                    return !(el == null | el == '');
                                });
                                arrayInputPlaceholder = arrayInputPlaceholder.filter(function (el) {
                                    return !(el == null | el == '');
                                });


                                var aktivniFiltry = {};
                                filtrName.forEach(function (key, idx) {
                                    aktivniFiltry[key] = filtrData[idx]
                                });
                                var zadaneFiltry = {};
                                array.forEach(function (key, idx) {
                                    zadaneFiltry[key] = arrayInput[idx]
                                });

                                result = Object.assign({}, aktivniFiltry, zadaneFiltry);
                                filtrName = Object.keys(result);
                                filtrData = Object.keys(result).map(function (itm) {
                                    return result[itm];
                                });

                                filtrColkey = arrayUnique(filtrColkey.concat(arrayInputPlaceholder));
                                if (vars.ShowFilteredData != "" && typeof vars.ShowFilteredData == "function") {
                                    var filtry = {};
                                    filtrColkey.forEach((key, i) => filtry[key] = filtrData[i]);
                                    Object.keys(filtry).map(function (colkey) {
                                        vsechny_sloupce.find(function (sloupec) {
                                            var colkeyToUse = colkey;
                                            if (colkey.endsWith("_do") || colkey.endsWith("_od")) {
                                                colkeyToUse = colkey.substring(0, colkey.length - 3)
                                            }
                                            if (sloupec.colkey == colkeyToUse)
                                                if (colkey !== sloupec.label) {
                                                    var filtrName = sloupec.label;

                                                    if (colkey.endsWith("_od")) {
                                                        filtrName += " - od";
                                                    } else if (colkey.endsWith("_do")) {
                                                        filtrName += " - do";
                                                    }
                                                    Object.defineProperty(filtry, filtrName, Object.getOwnPropertyDescriptor(filtry, colkey));
                                                    delete filtry[colkey];
                                                }
                                            return 0;
                                        });
                                        return 0;
                                    });
                                    vars.ShowFilteredData(filtry);
                                }
                            }
                            ajaxToUse = {
                                'async': true,
                                'type': 'POST',
                                'global': false,
                                'url': filtrUrl,
                                'dataType': "json",
                                'beforeSend': function beforeSend(xhr, settings) {
                                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                                },
                                'data': {
                                    'filtry': JSON.stringify(filtrData),
                                    'sloupce': JSON.stringify(filtrName),
                                    'datatable': vars.dtview_name,
                                    'neignorovat': +document.getElementById("dtNeIgnorovatVelka").checked,
                                    'zobrazeni': vybrane_zobrazeni
                                },
                                'complete': function () {

                                    if (document.getElementById('dtBtnZrusitFiltr2') == null) {
                                        document.getElementById('DtToolbar').insertAdjacentHTML('beforeend', '<button id="dtBtnZrusitFiltr2" class="btn btn-danger" style="display:none; margin-left: 4px">Zrušit filtry</button>');
                                        jQuery('#dtBtnZrusitFiltr2').on('click', dtBtnZrusitFiltrOnClick);
                                    }

                                    document.getElementById('dtBtnZrusitFiltr2').style.display = 'inline';
                                    document.getElementById('dtBtnZrusitFiltr').style.display = 'block';
                                    zobrazovatFiltry = false;
                                    jQuery('#FiltrModal').modal('hide');
                                }


                            };
                            break;
                        case "formular"://refresh přišel z filtrovani pred nactenim
                            var splnuje = false;
                            var arrayInput = []; //Seznam zadaných hodnot (Michal)

                            var arrayInputPlaceholder = []; //Colkeys
                            namePredchozi = "";
                            index = 0;
                            jQuery("#predchazejiciFiltr").children().find("input[type='text']").each(function () {
                                if (this.classList.contains("dtviewFilter-date")) {
                                    if (namePredchozi != jQuery(this).attr('name').substr(0, jQuery(this).attr('name').length - 3)) {
                                        arrayInputPlaceholder[index] = jQuery(this).attr('name');

                                        arrayInput[index] = [jQuery(this).val()];
                                        namePredchozi = jQuery(this).attr('name').substr(0, jQuery(this).attr('name').length - 3);
                                    } else {
                                        index--;
                                        arrayInput[index].push(jQuery(this).val());
                                    }
                                } else {
                                    arrayInputPlaceholder[index] = jQuery(this).attr('name');
                                    arrayInput[index] = jQuery(this).val();
                                }
                                if (!splnuje && jQuery(this).val().length > 0) {
                                    splnuje = true;
                                }
                                index++;
                            });
                            var array = [];
                            if (splnuje) {
                                if (zobrazovatFiltry) {
                                    document.getElementById("dtBtnZrusitFiltr2").style.display = "inline";
                                } //pro kazdy zaznam zkontroluje jestli plni všechny zadané podmínky

                                array = vybrane_sloupce.filter(function (sloupec) {
                                    return sloupec.filterable == 1;
                                }); //Seznam parametrů zadaných hodnot (jmeno)

                                array = array.map(function (e) {
                                    return e.colkey;
                                });

                                for (var i = 0; i < array.length; ++i) {
                                    if (arrayInput[i].length <= 0 | (arrayInput[i][0] == "" && arrayInput[i][1] == "")) {
                                        arrayInput.splice(i, 1);
                                        array.splice(i, 1);
                                        arrayInputPlaceholder.splice(i, 1);
                                        --i;
                                    }
                                }
                                arrayInput = arrayInput.filter(function (el) {
                                    return !(el == null | el == '');
                                });
                                arrayInputPlaceholder = arrayInputPlaceholder.filter(function (el) {
                                    return !(el == null | el == '');
                                });
                                var aktivniFiltry = {};
                                filtrName.forEach(function (key, idx) {
                                    aktivniFiltry[key] = filtrData[idx]
                                });
                                var zadaneFiltry = {};
                                array.forEach(function (key, idx) {
                                    zadaneFiltry[key] = arrayInput[idx]
                                });

                                result = Object.assign({}, aktivniFiltry, zadaneFiltry);
                                filtrName = Object.keys(result);

                                filtrData = Object.keys(result).map(function (itm) {
                                    return result[itm];
                                });
                                filtrColkey = arrayUnique(filtrColkey.concat(arrayInputPlaceholder));
                                if (vars.ShowFilteredData != "" && typeof vars.ShowFilteredData == "function") {
                                    var filtry = {};
                                    filtrColkey.forEach((key, i) => filtry[key] = filtrData[i]);
                                    Object.keys(filtry).map(function (colkey) {
                                        vsechny_sloupce.find(function (sloupec) {
                                            var colkeyToUse = colkey;
                                            if (colkey.endsWith("_do") || colkey.endsWith("_od")) {
                                                colkeyToUse = colkey.substring(0, colkey.length - 3)
                                            }
                                            if (sloupec.colkey == colkeyToUse)
                                                if (colkey !== sloupec.label) {
                                                    var filtrName = sloupec.label;

                                                    if (colkey.endsWith("_od")) {
                                                        filtrName += " - od";
                                                    } else if (colkey.endsWith("_do")) {
                                                        filtrName += " - do";
                                                    }
                                                    Object.defineProperty(filtry, filtrName, Object.getOwnPropertyDescriptor(filtry, colkey));
                                                    delete filtry[colkey];
                                                }
                                            return 0;
                                        });
                                        return 0;
                                    });
                                    vars.ShowFilteredData(filtry);
                                }
                            }
                            ajaxToUse = {
                                'async': true,
                                'type': 'POST',
                                'global': false,
                                'url': filtrUrl,
                                'dataType': "json",
                                'beforeSend': function beforeSend(xhr, settings) {
                                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                                },
                                'data': {
                                    'filtry': JSON.stringify(filtrData),
                                    'sloupce': JSON.stringify(filtrName),
                                    'datatable': vars.dtview_name,
                                    'neignorovat': +document.getElementById("FiltrdtNeIgnorovatVelka").checked,
                                    'zobrazeni': vybrane_zobrazeni
                                },
                                'complete': function () {
                                    values = validate.collectFormValues(document.getElementById('predchazejiciFiltr'), {nullify: false});
                                    for (key in values) {
                                        document.forms["datatableFiltr"][key].value = values[key];
                                    }
                                    if (document.getElementById('dtBtnZrusitFiltr2') == null) {
                                        document.getElementById('DtToolbar').insertAdjacentHTML('beforeend', '<button id="dtBtnZrusitFiltr2" class="btn btn-danger" style="display:none; margin-left: 4px">Zrušit filtry</button>');
                                        jQuery('#dtBtnZrusitFiltr2').on('click', dtBtnZrusitFiltrOnClick);
                                    }

                                    document.getElementById('dtBtnZrusitFiltr2').style.display = 'inline';
                                    document.getElementById('dtBtnZrusitFiltr').style.display = 'block';
                                    document.getElementById(vars.elementId).innerHTML = "";
                                }
                            };

                            break;
                        default: //defaultni refresh
                            ajaxToUse = {
                                'async': true,
                                'type': 'POST',
                                'beforeSend': function beforeSend(xhr, settings) {
                                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                                },
                                'global': false,
                                'url': dataUrl,
                                'dataType': "json",
                                'data': {
                                    'filtry': +zobrazovatFiltry,
                                    'datatable': vars.dtview_name,
                                    'zobrazeni': vybrane_zobrazeni
                                },
                                'complete': function () {
                                    jQuery('#dtFiltrModalBody').children('form').children().children('input[type="text"]').each(function (index) {
                                        jQuery(this).val("");
                                    });
                                    if (document.getElementById('dtBtnZrusitFiltr2') == null) {
                                        document.getElementById('DtToolbar').insertAdjacentHTML('beforeend', '<button id="dtBtnZrusitFiltr2" class="btn btn-danger" style="display:none; margin-left: 4px">Zrušit filtry</button>');
                                        jQuery('#dtBtnZrusitFiltr2').on('click', dtBtnZrusitFiltrOnClick);
                                    }
                                    document.getElementById('dtBtnZrusitFiltr2').style.display = 'none';
                                    document.getElementById('dtBtnZrusitFiltr').style.display = 'none';
                                    document.getElementById(vars.elementId).innerHTML = "";
                                }
                            };
                            break;
                    }
                    tabulka = jQuery('#dtView').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": ajaxToUse,
                        "dom": '<"row"<"col-12"<"text-center"<"#DtToolbar">>><"col-3">>rt<"mt-3"lp>',
                        "columns": columnKeys,
                        "scrollX": true,
                        "language": {
                            "lengthMenu": "Zobrazuje se _MENU_ záznámu na stránku",
                            "zeroRecords": "Nic nebylo nalezeno",
                            "info": "Strana _PAGE_ z _PAGES_",
                            "infoEmpty": "Žádné záznamy",
                            "infoFiltered": "(filtrováno z _MAX_ záznamů)",
                            "search": "Hledat",
                            "paginate": {
                                "first": "První",
                                "last": "Poslední",
                                "next": "Další",
                                "previous": "Předchozí"
                            }
                        }
                    });
                    var detailRows = [];
                    var detailRows1 = [];

                    if (typeof childRowFormat === "function") {
                        jQuery('#dtView tbody tr').not('.dtviewChildInformace').click(function () {
                            var tr = jQuery(this).closest('tr');
                            var row = tabulka.row(tr);
                            var idx = jQuery.inArray(tr.attr('id'), detailRows);

                            if (row.child.isShown()) {
                                tr.removeClass('details');
                                row.child.hide(); // Remove from the 'open' array

                                detailRows.splice(idx, 1);
                            } else {
                                tr.addClass('details');
                                row.child(childRowFormat(row.data())).show(); // Add to the 'open' array

                                if (idx === -1) {
                                    detailRows.push(tr.attr('id'));
                                }
                            }
                        });
                    }

                    tabulka.on('draw', function () {
                        jQuery.each(detailRows, function (i, id) {
                            jQuery('#' + id + ' td.details-control').trigger('click');
                        });
                    });

                    function dtPridatTlacitka() {
                        vybrane_sloupce.filter(function (sloupec) {
                            return sloupec.fmt_funkce.length > 0;
                        }).forEach(function (sloupec) {
                            columnKeys[vybrane_sloupce.indexOf(sloupec)].render = window[sloupec.fmt_funkce];
                            columnKeys[vybrane_sloupce.indexOf(sloupec)].title = sloupec.label;
                        });
                    }

                    if (vars.showSettings == 1) {
                        jQuery("#DtToolbar").html("<select id='dataTableSelect'></select> \
                            <button onclick=\"jQuery('#NastaveniModal').modal('show');\" class=\"btn btn-primary\"><i class=\"fa fa-cogs\"></i> Nastaveni</button>  \
                            <button onclick=\"jQuery('#FiltrModal').modal('show');\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i> Filtr</button>");
                    } else {
                        jQuery("#DtToolbar").html("<select id='dataTableSelect'></select> \
                            <button onclick=\"jQuery('#FiltrModal').modal('show');\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i> Filtr</button>");
                    }


                    vsechny_zobrazeni.forEach(function (item, index) {
                        if (item.id != vybrane_zobrazeni) {
                            if (item.user_id == json.dtview_cfg.user_id) {
                                document.getElementById('dataTableSelect').insertAdjacentHTML("beforeend", "<option value='" + item.id + "' style='background:#84b9ff'>" + item.nazev + "</option>");
                            } else if (item.vychozi) {
                                document.getElementById('dataTableSelect').insertAdjacentHTML("beforeend", "<option value='" + item.id + "' style='background:#cbcbcb'>" + item.nazev + "</option>");
                            } else{
                                document.getElementById('dataTableSelect').insertAdjacentHTML("beforeend", "<option value='" + item.id + "' style='background:#ffffff'>" + item.nazev + "</option>");
                            }
                        } else {
                                document.getElementById('dataTableSelect').insertAdjacentHTML("beforeend", "<option value='" + item.id + "'selected style='background:#84ff98'>" + item.nazev + "</option>");
                        }
                    });
                    jQuery('#dataTableSelect').on('change', dataTableOnChange);
                }

                function ulozitZmenyZobrazeni() {
                    var dataNaUlozeni = [];
                    var razeni = "";
                    jQuery("#dtVybraneRadky li ").each(function (index) {
                        var idSloupce = jQuery(this).attr('id');
                        var colkeySloupce = idSloupce.split(/_(.+)/)[1];

                        if (jQuery(this).hasClass("dtRazeni-up")) {
                            razeni = colkeySloupce + " " + "asc";
                        } else if (jQuery(this).hasClass("dtRazeni-down")) {
                            razeni = colkeySloupce + " " + "desc";
                        }

                        var danySloupec = vsechny_sloupce.filter(function (sloupec) {
                            return sloupec.colkey == colkeySloupce;
                        })[0];
                        dataNaUlozeni.push({
                            "id": index + 1,
                            "col_id": danySloupec.id,
                            "dv_id": parseInt(vybrane_zobrazeni),
                            "poradi": index + 1
                        });
                    });
                    var chVerejna = document.getElementById("dtNastaveniVerejneCheckBox").checked;
                    jQuery.ajax({
                        beforeSend: function beforeSend(xhr, settings) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        },
                        type: 'POST',
                        url: dtview_urlUlozit,
                        data: {
                            'dv_id': vybrane_zobrazeni,
                            'dv_nazev': document.getElementById('dtNazevZobrazeni').value,
                            'dv_dvcol': JSON.stringify(dataNaUlozeni),
                            'dv_dt': vybrany_table,
                            'dv_verejne': chVerejna | 0,
                            'dv_razeni': razeni,
                            'filtry': JSON.stringify(dvSkupinyFiltru)
                        },
                        dataType: 'json',
                        success: function success(data) {
                            if (data) {
                                alert("Změna byla uložena");
                                location.reload(true);
                            } else {
                                alert("Změna nebyla uložena");
                            }


                        }
                    });
                }

                function ulozitNoveZobrazeni() {
                    var dataNaUlozeni = [];
                    var razeni = "";
                    jQuery("#dtVybraneRadky li ").each(function (index) {
                        var idSloupce = jQuery(this).attr('id');
                        var colkeySloupce = idSloupce.split(/_(.+)/)[1];

                        if (jQuery(this).hasClass("dtRazeni-up")) {
                            razeni = colkeySloupce + " " + "asc";
                        } else if (jQuery(this).hasClass("dtRazeni-down")) {
                            razeni = colkeySloupce + " " + "desc";
                        }

                        var danySloupec = vsechny_sloupce.filter(function (sloupec) {
                            return sloupec.colkey == colkeySloupce;
                        })[0];
                        dataNaUlozeni.push({
                            "id": index + 1,
                            "col_id": danySloupec.id,
                            "dv_id": parseInt(vybrane_zobrazeni),
                            "poradi": index + 1
                        });
                    });
                    var chVerejna = document.getElementById("dtNastaveniVerejneCheckBox").checked;
                    jQuery.ajax({
                        beforeSend: function beforeSend(xhr, settings) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        },
                        type: 'POST',
                        url: dtview_urlVytvorit,
                        data: {
                            'dv_dvcol': JSON.stringify(dataNaUlozeni),
                            'dv_nazev': document.getElementById('dtNazevZobrazeni').value,
                            'dv_dt': vybrany_table,
                            'dv_verejne': chVerejna | 0,
                            'dv_razeni': razeni,
                            'filtry': JSON.stringify(dvSkupinyFiltru)
                        },
                        dataType: 'json',
                        success: function success(data) {
                            if (data) {
                                alert("Zobrazení bylo vytvořeno");
                                location.reload(true);
                            } else {
                                alert("Zobrazení nebylo vytvořeno");
                            }


                        }
                    });
                }

                jQuery('#dtBtnVymazatZobrazeni').on('click', vymazatZobrazeni);
                jQuery('#dtBtnUlozitZmeny').on('click', ulozitZmenyZobrazeni);
                jQuery('#dtBtnUlozitZobrazeni').on('click', ulozitNoveZobrazeni);

                function vymazatZobrazeni() {
                    if (confirm("Vážně chcete trvale vymazat zobrazení: " + json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].nazev)) {
                        jQuery.ajax({
                            beforeSend: function beforeSend(xhr, settings) {
                                xhr.setRequestHeader("X-CSRFToken", csrftoken);
                            },
                            type: 'POST',
                            url: dtview_urlVymazat,
                            data: {
                                'dv_id': vybrane_zobrazeni
                            },
                            dataType: 'json',
                            success: function success(data) {
                                if (data) {
                                    alert("Zobrazení bylo odstraněno");
                                    location.reload(true);
                                } else {
                                    alert("Odstranění nebylo úspěšné");
                                }
                            }
                        });
                    }
                } //Drag and Drop


                var draggedElement = null;
                var dragoverElement = null;

                function onDragStart(element) {
                    element.originalEvent.dataTransfer.setData('text', 'anything');
                    draggedElement = element.target;
                    draggedElement.style.opacity = '0.6';
                }

                function onDragEnd(element) {
                    element.target.style.opacity = '1';
                }

                function onDragEnter(element) {
                    element.preventDefault();
                    if (element.target.id == "dtZbyleRadky" || element.target.id == "dtVybraneRadky") {
                        dragoverElement = element.target;
                    } else {
                        var ua = window.navigator.userAgent;
                        var msie = ua.indexOf("MSIE ");

                        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) //Zjistí zda se jedno o internet explorer
                        {
                        } else {
                            element.target.style.outline = '1px solid black';
                            element.target.style.borderTop = '2px dashed blue';
                        }

                        dragoverElement = element.target;
                    }
                }

                function onDragLeave(element) {
                    element.target.style.outline = '1px solid black';
                    element.target.style.borderTop = 'none';
                }

                function onDrop(element) {
                    element.preventDefault();
                    draggedElement.style.opacity = '1';

                    if (dragoverElement.id != "dtZbyleRadky" && dragoverElement.id != "dtVybraneRadky") {
                        dragoverElement.style.outline = '1px solid black';
                        dragoverElement.style.borderTop = 'none';
                    }

                    if (draggedElement != dragoverElement) {
                        draggedElement.parentNode.removeChild(draggedElement);

                        if (dragoverElement.id == "dtZbyleRadky" || dragoverElement.id == "dtVybraneRadky") {
                            dragoverElement.insertAdjacentHTML('beforeend', draggedElement.outerHTML);
                        } else {
                            dragoverElement.insertAdjacentHTML('beforebegin', draggedElement.outerHTML);
                        }
                    }

                    draggedElement = null;
                    dragoverElement = null;
                    jQuery('#dtVybraneRadky li').unbind();
                    jQuery('#dtZbyleRadky li').unbind();
                    jQuery('#dtVybraneRadky li').on('click', razeniClick);
                    jQuery('#dtZbyleneRadky li').on('click', razeniClick);
                    jQuery('#dtVybraneRadky li').on('dragstart', onDragStart);
                    jQuery('#dtVybraneRadky li').on('dragend', onDragEnd);
                    jQuery('#dtZbyleRadky li').on('dragstart', onDragStart);
                    jQuery('#dtZbyleRadky li').on('dragend', onDragEnd);
                    jQuery('#dtVybraneRadky').on('dragenter', onDragEnter);
                    jQuery('#dtZbyleRadky').on('dragenter', onDragEnter);
                    jQuery('#dtVybraneRadky li').on('dragleave', onDragLeave);
                    jQuery('#dtZbyleRadky li').on('dragleave', onDragLeave);
                }

                function onDragOver(element) {
                    element.preventDefault();
                } //Změní řádky dostupné v nastavení po změně view


                function aktualizujNastaveni() {
                    document.getElementById('dtNazevZobrazeni').placeholder = json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].nazev;

                    if (json.dtview_cfg.user_id == json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].user_id) {
                        document.getElementById('dtBtnUlozitZmeny').style.display = "block";
                        document.getElementById('dtBtnVymazatZobrazeni').style.display = "block";
                    } else {
                        document.getElementById('dtBtnUlozitZmeny').style.display = "none";
                        document.getElementById('dtBtnVymazatZobrazeni').style.display = "none";
                    }

                    var element = document.getElementById('dtVybraneRadky');
                    element.innerHTML = "";

                    for (var i = 0; i < vybrane_sloupce.length; ++i) {
                        var sloupec = vybrane_sloupce[i];
                        element.insertAdjacentHTML("beforeend", "<li class='text-center' id='dtNastaveni_" + sloupec.colkey + "' draggable='true' style='list-style-type: none;cursor: move; outline: 1px solid black; margin-bottom:8px;padding:5px;font-weight: bold; font-style: italic'>" + sloupec.label + "</li");
                    }

                    element = document.getElementById('dtZbyleRadky');
                    element.innerHTML = "";

                    for (var _i3 = 0; _i3 < vsechny_sloupce.length; ++_i3) {
                        if (vybrane_sloupce.indexOf(vsechny_sloupce[_i3]) == -1) {
                            var _sloupec = vsechny_sloupce[_i3];
                            element.insertAdjacentHTML("beforeend", "<li class='text-center' id='dtNastaveni_" + _sloupec.colkey + "' draggable='true' style='list-style-type: none;cursor: move; outline: 1px solid black;margin-bottom:8px;padding:5px;font-weight: bold; font-style: italic'>" + _sloupec.label + "</li");
                        }
                    }

                    if (json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].verejne == 1) {
                        jQuery('#dtNastaveniVerejneCheckBox').prop('checked', true);
                    } else {
                        jQuery('#dtNastaveniVerejneCheckBox').prop('checked', false);
                    }

                    jQuery('#dtVybraneRadky li').unbind();
                    jQuery('#dtZbyleRadky li').unbind();
                    jQuery('#dtVybraneRadky li').on('click', razeniClick);
                    jQuery('#dtZbyleneRadky li').on('click', razeniClick);
                    jQuery('#dtVybraneRadky li').on('dragstart', onDragStart);
                    jQuery('#dtVybraneRadky li').on('dragend', onDragEnd);
                    jQuery('#dtVybraneRadky').on('dragstart', onDragStart);
                    jQuery('#dtVybraneRadky li').on('dragend', onDragEnd);
                    jQuery('#dtZbyleRadky li').on('dragstart', onDragStart);
                    jQuery('#dtZbyleRadky li').on('dragend', onDragEnd);
                    jQuery('#dtVybraneRadky').on('dragenter', onDragEnter);
                    jQuery('#dtZbyleRadky').on('dragenter', onDragEnter);
                    jQuery('#dtVybraneRadky li').on('dragleave', onDragLeave);
                    jQuery('#dtZbyleRadky li').on('dragleave', onDragLeave);
                }

                if (vars.showSettings == 1) {
                    aktualizujNastaveni();
                }

                var elementRazeni = null;

                function razeniClick(element) {
                    if (elementRazeni != null && elementRazeni.id != element.target.id) {
                        if (elementRazeni.classList.contains("dtRazeni-up") || elementRazeni.classList.contains("dtRazeni-down")) {
                            elementRazeni.removeChild(elementRazeni.lastElementChild);
                            elementRazeni.classList.remove("dtRazeni-down");
                            elementRazeni.classList.remove("dtRazeni-up");
                            elementRazeni.style.backgroundColor = "white";
                        }

                        elementRazeni = element.target;
                    } else {
                        elementRazeni = element.target;
                    }

                    if (elementRazeni.classList.contains("dtRazeni-up")) {
                        elementRazeni.classList.remove("dtRazeni-up");
                        elementRazeni.classList.add("dtRazeni-down");
                        elementRazeni.removeChild(element.target.lastElementChild); //downloaded from https://icons8.com/

                        elementRazeni.insertAdjacentHTML("beforeend", "<img src='" + dataview_DOWN + "' alt = 'something' class='float-right' style='width:32px; height:32px;pointer-events: none'>");
                        elementRazeni.style.backgroundColor = "#edf6fc";
                    } else if (elementRazeni.classList.contains("dtRazeni-down")) {
                        elementRazeni.removeChild(element.target.lastElementChild);
                        elementRazeni.classList.remove("dtRazeni-down");
                        elementRazeni.style.backgroundColor = "white";
                        elementRazeni = null;
                    } else {
                        elementRazeni.classList.add("dtRazeni-up"); //downloaded from https://icons8.com/

                        elementRazeni.insertAdjacentHTML("beforeend", "<img src='" + dataview_UP + "' alt = 'something' class='float-right' style='width:32px; height:32px;pointer-events: none'>");
                        elementRazeni.style.backgroundColor = "#edf6fc";
                    }
                }

                jQuery('#dtVybraneRadky').on('dragover', onDragOver);
                jQuery('#dtZbyleRadky').on('dragover', onDragOver);
                jQuery('#dtVybraneRadky').on('drop', onDrop);
                jQuery('#dtZbyleRadky').on('drop', onDrop); //Aktualizuje vizuální část filtrů

                function aktualizujFiltry() {
                    var element = document.getElementById('datatableFiltr');
                    element.innerHTML = "";
                    vybrane_sloupce.filter(function (sloupec) {
                        return sloupec.filterable == 1;
                    }).forEach(function (item, index) {

                        if (item.typ == "date") {
                            if (item.konverze == "dateonly") {
                                element.insertAdjacentHTML("beforeend", "<div class='form-group'><input type='text' autocomplete='off' class='form-control' style='margin-bottom: 8px' placeholder='" + item.label + "' name='" + item.colkey + "' '><div class='messages'></div></div>");
                                jQuery(document.forms["datatableFiltr"]).children().children('input[name="' + item.colkey + '"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["datatableFiltr"], vars.constraints) || {};
                                        zobrazitErrory(
                                            jQuery(document.forms["datatableFiltr"][item.colkey])[0],
                                            errors[jQuery(document.forms["datatableFiltr"][item.colkey])
                                                .attr("name")]);
                                    }
                                });

                            } else {
                                element.insertAdjacentHTML("beforeend", '<div class="form-group"><input type="text" autocomplete="off" class="dtviewFilter-date form-control col-sm-5" style="margin-bottom:8px" name="' + item.colkey + '_od" placeholder = "' + item.label + '(od)" Formát: (dd.mm.YYYY nebo dd.mm)"' + '"/><div class="messages"></div></div><div class="form-group"><input type="text" autocomplete="off" class="dtviewFilter-date form-control col-sm-5" style="margin-bottom:8px" name="' + item.colkey + '_do" placeholder = "' + item.label + '(do)" Formát: (dd.mm.YYYY nebo dd.mm)"' + '"/><div class="messages"></div></div>');

                                jQuery(document.forms["datatableFiltr"]).children().children('input[name="' + item.colkey + '_od"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onShow: function (ct) {
                                        this.setOptions({
                                            maxDate: jQuery('#datatableFiltr').children().children("input[name=" + item.colkey + "_do]").val() ? jQuery('#datatableFiltr').children().children("input[name=" + item.colkey + "_do]").val() : false,
                                            formatDate: 'd.m.Y',
                                        })
                                    },
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["datatableFiltr"], vars.constraints) || {};
                                        zobrazitErrory(jQuery(document.forms["datatableFiltr"][item.colkey + '_od'])[0], errors[jQuery(document.forms["datatableFiltr"][item.colkey + '_od']).attr("name")]);
                                    }
                                });
                                jQuery(document.forms["datatableFiltr"]).children().children('input[name="' + item.colkey + '_do"]').datetimepicker({
                                    timepicker: false,
                                    format: 'd.m.Y',
                                    validateOnBlur: true,
                                    onShow: function (ct) {
                                        this.setOptions({
                                            minDate: jQuery('#datatableFiltr').children().children("input[name=" + item.colkey + "_od]").val() ? jQuery('#datatableFiltr').children().children("input[name=" + item.colkey + "_od]").val() : false,
                                            formatDate: 'd.m.Y',
                                        })
                                    },
                                    onClose: function (ct) {
                                        var errors = validate(document.forms["datatableFiltr"], vars.constraints) || {};
                                        zobrazitErrory(jQuery(document.forms["datatableFiltr"][item.colkey + '_do'])[0], errors[jQuery(document.forms["datatableFiltr"][item.colkey + '_do']).attr("name")]);
                                    }
                                });
                            }
                        } else {
                            element.insertAdjacentHTML("beforeend", "<div class='form-group'><input type='text' class='form-control' style='margin-bottom: 8px' placeholder='" + item.label + "' name='" + item.colkey + "' '><div class='messages'></div></div>");
                        }
                    });
                    /*jQuery('.dtviewFilter-date').datetimepicker({
                        timepicker: false,
                        format: 'd.m.Y',
                        validateOnBlur: true,
                        onShow: function (ct) {
                            console.log(154);
                            console.log(jQuery(this));
                            this.setOptions({
                                maxDate: jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
                            })
                        },
                    });*/
                    jQuery.datetimepicker.setLocale('cs');
                    if (vars.checkboxCaseSensitive == 1) {
                        element.insertAdjacentHTML("beforeend",
                            '<label class="form-check-label" style="margin-right: 5px" for="dtNeIgnorovatVelka" > Rozlišovat velká a malá písmena</label>\
                                <input type="checkbox" id="dtNeIgnorovatVelka" value="neignorovat">');
                    } else {
                        element.insertAdjacentHTML("beforeend",
                            '<label class="form-check-label" style="margin-right: 5px; display:none" for="dtNeIgnorovatVelka" > Rozlišovat velká a malá písmena</label>\
                                <input type="checkbox" id="dtNeIgnorovatVelka" style="display:none" value="neignorovat">');
                    }

                    element.insertAdjacentHTML("beforeend", '<button id="dtBtnZrusitFiltr" class="btn btn-secondary">Zrušit filtry</button>\
                        <button id="dtBtnFiltr" class="btn btn-primary" type="submit" >Aplikovat filtry</button>');

                    var inputs = document.forms["datatableFiltr"].querySelectorAll("input[type='text']", "textarea", "select");
                    for (let i = 0; i < inputs.length; ++i) {
                        inputs.item(i).addEventListener("change", function () {
                            var errors = validate(document.forms["datatableFiltr"], vars.constraints) || {};
                            zobrazitErrory(this, errors[this.name]);
                        })
                    }
                }

                aktualizujFiltry(); //Filtr
                window.dtBtnFiltrOnClick = function dtBtnFiltrOnClick() {
                    tabulka.destroy();
                    document.getElementById('dtView').innerHTML = "";
                    zobrazovatFiltry = false;
                    jQuery('#FiltrModal').modal('hide');
                    filtrColkey = [];
                    filtrData = [];
                    filtrName = [];
                    inicializace_tabulky("filtr");
                };

                function dtBtnZrusitFiltrOnClick() {
                    tabulka.destroy();
                    filtrColkey = [];
                    filtrData = [];
                    filtrName = [];

                    document.getElementById('dtView').innerHTML = "";
                    zobrazovatFiltry = false;
                    document.getElementById('dtBtnZrusitFiltr').style.display = 'none';
                    document.getElementById('dtView').innerHTML = "";
                    jQuery('#dtFiltrModalBody').children('form').children('input[type="text"]').each(function (index) {
                        jQuery(this).val("");
                    });
                    jQuery('#FiltrModal').modal('hide');
                    inicializace_tabulky("nacteni");
                    if (vars.ShowFilteredData != "" && typeof vars.ShowFilteredData == "function") {
                        vars.ShowFilteredData({});
                    }
                }

                function dataTableOnChange() { // TODO
                    var vybrana_value = this.value;
                    vybrane_zobrazeni = vybrana_value;
                    nastav_vybrane_sloupce();
                    nastav_collumkey();
                    if (vars.showSettings == 1) {
                        aktualizujNastaveni();
                        dvSkupinyFiltru = [];
                    var str = json.dtview_dv.filter(function (zobrazeni) {
                        return zobrazeni.id == vybrane_zobrazeni;
                    })[0].filtry;
                    str.split(";").forEach(function (_skupina) {
                        var skupina = []
                        _skupina.split(",").forEach(function (filtr) {
                            var objekt;
                            if (filtr.indexOf("<=")!=-1) {
                                objekt = filtr.split(/(<=)/g);
                            }
                            else if(filtr.indexOf(">=")!=-1)
                            {
                                objekt = filtr.split(/(>=)/g);
                            }
                            else if(filtr.indexOf("*<")!=-1)
                            {
                                objekt = filtr.split(/(\*<)/g);
                            }
                            else if(filtr.indexOf("*>")!=-1)
                            {
                                objekt = filtr.split(/(\*>)/g);
                            }
                            else if(filtr.indexOf("*=")!=-1)
                            {
                                objekt = filtr.split(/(\*=)/g);
                            }
                            else
                            {
                                objekt = filtr.split(/([=|<|>])/g);
                            }
                            console.log(objekt);
                            if(objekt.length>1)
                            {
                                skupina.push({"sloupec":objekt[0], "operace":objekt[1], "hodnota":objekt[2]});
                            }

                        })
                        if(skupina.length>0)
                        {
                            dvSkupinyFiltru.push(skupina);
                        }

                    })// TODO
                    console.log(dvSkupinyFiltru);
                    }
                    tabulka.destroy();
                    document.getElementById('dtView').innerHTML = "";
                    var splnuje = false;
                    jQuery("#datatableFiltr").children().find("input[type='text']").each(function (index) {
                        if (!splnuje && (jQuery(this).val() !== "" || jQuery(this).val()[0] !== undefined)) {
                            splnuje = true;
                        }
                    });

                    if (splnuje || filtrData.length > 0) {
                        inicializace_tabulky("filtr");
                    } else {
                        inicializace_tabulky("nacteni");
                    }
                    aktualizujFiltry();
                    let jeDatum = -1;
                    jQuery('#datatableFiltr').children().children('input[type="text"]').each(function () {
                        if (jeDatum != -1) {
                            $(this)[0].value = filtrData[jeDatum][1];
                            jeDatum = -1;
                        }
                        for (let i = 0; i < filtrColkey.length; ++i) {

                            if ($(this)[0].name === filtrColkey[i]) {
                                if (filtrColkey[i].endsWith("_od")) {
                                    $(this)[0].value = filtrData[i][0];
                                    jeDatum = i;
                                } else if (filtrColkey[i].endsWith("_do")) {
                                    $(this)[0].value = filtrData[i][1];
                                } else {
                                    $(this)[0].value = filtrData[i];
                                }
                            }
                        }

                    });
                   createConfig()
                }

                //DV filtry
                //TODO


                if(vars.showSettings == 1) {
                    var editovanaSkupina = [];
                document.getElementById("btnFiltryZrusit").onclick = function(){
                    document.getElementById("dvFiltr").innerHTML = "";
                    vybranaSkupina = 0;
                    //TODO zde vymazat skupiny
                    //TODO rozdělit na původní a nové filtry
                    jQuery('#dvSkupinaFiltruModal').modal('hide');
                    jQuery('#NastaveniModal').modal('show');
                    console.log("WTF");
                    console.log(dvSkupinyFiltru);
                    console.log(dvSkupinyFiltru_pridat);
                    dvSkupinyFiltru_pridat = JSON.parse(JSON.stringify(dvSkupinyFiltru));
                    console.log(dvSkupinyFiltru_pridat);
                };
                    document.getElementById("btnFiltryZrusit2").onclick = function(){
                        jQuery('#dvJednotlivyFiltrModal').modal('hide');
                        console.log(dvSkupinyFiltru);
                    console.log(dvSkupinyFiltru_pridat);

                    console.log(editovanaSkupina);
                    if(((Array)(dvSkupinyFiltru_pridat[vybranaSkupina-1])).length >0)
                    {
                        dvSkupinyFiltru_pridat[vybranaSkupina-1] = JSON.parse(JSON.stringify(editovanaSkupina));
                    }
                    else{
                        dvSkupinyFiltru_pridat.splice(vybranaSkupina-1,1);
                    }

                };
                    //Uložit skupiny
                document.getElementById("btnFiltryPridat").onclick = function(){
                    vybranaSkupina = 0;
                    document.getElementById("dvFiltr").innerHTML = "";
                    jQuery('#dvSkupinaFiltruModal').modal('hide');
                    jQuery('#NastaveniModal').modal("show");
                    console.log(dvSkupinyFiltru);
                    console.log(dvSkupinyFiltru_pridat);
                    dvSkupinyFiltru = JSON.parse(JSON.stringify(dvSkupinyFiltru_pridat));
                };
                //Uložit skupinu
                document.getElementById("btnFiltryPridat2").onclick = function(){
                        jQuery('#dvJednotlivyFiltrModal').modal('hide');
                        $("#dvJednotlivyFiltr").children().each(function () {var el = $(this).children("span");
                            dvSkupinyFiltru_pridat[vybranaSkupina - 1][$(this).attr("poziceFiltru") - 1]['sloupec'] = $(el).children(".dvSelectPole").val();
                            dvSkupinyFiltru_pridat[vybranaSkupina - 1][$(this).attr("poziceFiltru") - 1]['operace'] = $(el).children(".dvSelectOperator").val();
                            dvSkupinyFiltru_pridat[vybranaSkupina - 1][$(this).attr("poziceFiltru") - 1]['hodnota'] = $(el).children(".dvHodnota").val();
                            console.log("///////////////////////*****************");
                            console.log(dvSkupinyFiltru);
                            document.getElementById("dvJednotlivyFiltr").innerHTML = "";
                        });
                        document.getElementById("dvJednotlivyFiltr").innerHTML = "";
                        console.log(dvSkupinyFiltru);
                    console.log(dvSkupinyFiltru_pridat);
                };


                    $('#dvPridatSkupinu').on('click', function () {
                        dvPridatSkupinuFiltru();
                    });
                    $('#dvPridatFiltr').on('click', dvPridatJednotlivyFiltr);
                    console.log(optionsPole);

                    function dvPridatJednotlivyFiltr() {
                        dvSkupinyFiltru_pridat[vybranaSkupina - 1].push({});
                        var filtrString = '<div class="row bg-info dvJednotlivy" poziceFiltru="' + dvSkupinyFiltru_pridat[vybranaSkupina - 1].length + '" style="margin: 5px;"><span ><select class="dvSelectPole" style="margin-right: 5px;"></select><select class="dvSelectOperator" style="margin-right: 5px;"></select><input type="text" class="dvHodnota"></span><button type = "button" class= "close dvFiltrClose" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        document.getElementById("dvJednotlivyFiltr").insertAdjacentHTML('beforeend', filtrString);
                        $('.dvFiltrClose').unbind();
                        $('.dvFiltrClose').on('click', dvFiltrCloseClick);
                        $(".dvSelectPole").each(function () {
                            if ($(this)[0].options.length == 0) {
                                console.log(optionsPole);
                                for (var i = 0, l = optionsPole.length; i < l; i++) {
                                    var option = optionsPole[i];
                                    $(this)[0].options.add(new Option(option.text, option.value, option.selected));
                                }
                            }
                        });
                        $(".dvSelectOperator").each(function () {
                            if ($(this)[0].options.length == 0) {
                                for (var i = 0, l = optionsOperators.length; i < l; i++) {
                                    var option = optionsOperators[i];

                                    $(this)[0].options.add(new Option(option.text, option.value, option.selected));
                                }
                            }
                        });

                    }
                    console.log("WTsadasdF");
                    console.log("abc");
                    function dvPridatSkupinuFiltru() {
                        dvSkupinyFiltru_pridat.push([]);
                        var filtrString = '<div class="row bg-info dvSkupinaFiltru" poziceFiltru="' + dvSkupinyFiltru_pridat.length + '" style="margin: 5px;cursor: pointer;"><span>Skupina filtrů - ' + dvSkupinyFiltru_pridat.length + '</span><button type = "button" class= "close dvFiltryClose" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        document.getElementById("dvFiltr").insertAdjacentHTML('beforeend', filtrString);
                        $('.dvSkupinaFiltru').unbind();
                        $('.dvSkupinaFiltru').on('click', dvSkupinaFiltruClick);

                        $('.dvFiltrClose').unbind();
                        $('.dvFiltrClose').on('click', dvFiltrCloseClick);
                        $('.dvFiltryClose').unbind();
                        $('.dvFiltryClose').on('click', dvFiltryCloseClick);
                    }



                    function dvFiltrCloseClick() {
                        console.log(787878787878);
                        $(this).parent().remove();
                        var indexToRemove = $(this).parent().attr("poziceFiltru");
                        $("#dvJednotlivyFiltr").children().each(function () {
                            if ($(this).attr("poziceFiltru") > indexToRemove) {
                                $(this).attr("poziceFiltru", Number($(this).attr("poziceFiltru")) - 1);
                                var idSkupiny = $(this).attr("poziceFiltru");
                                //$(this)[0].innerHTML = '<div class="row bg-info dvSkupinaFiltru" poziceFiltru="' + dvSkupinyFiltru.length + '" style="margin: 5px;cursor: pointer;"><span>Filtry - ' + dvSkupinyFiltru.length + '</span><button type = "button" class= "close dvFiltryClose" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                            }
                        });
                        console.log("dvSkupinyFiltru");
                    console.log(dvSkupinyFiltru);
                    console.log("dvSkupinyFiltru_pridat");
                    console.log(dvSkupinyFiltru_pridat);
                        dvSkupinyFiltru_pridat[vybranaSkupina - 1].splice(indexToRemove - 1, 1);
                        console.log("dvSkupinyFiltru");
                    console.log(dvSkupinyFiltru);
                    console.log("dvSkupinyFiltru_pridat");
                    console.log(dvSkupinyFiltru_pridat);
                        $('.dvFiltrClose').unbind();
                        $('.dvFiltrClose').on('click', dvFiltrCloseClick);
                        return false;
                    }

                    function dvFiltryCloseClick() {
                        console.log(11111111);
                        $(this).parent().remove();
                        var indexToRemove = $(this).parent().attr("poziceFiltru");
                        $("#dvFiltr").children().each(function () {
                            if ($(this).attr("poziceFiltru") > indexToRemove) {
                                $(this).attr("poziceFiltru", Number($(this).attr("poziceFiltru")) - 1);
                                var idSkupiny = $(this).attr("poziceFiltru");
                                $(this)[0].innerHTML = '<span id="' + idSkupiny + '">Skupina filtrů - ' + idSkupiny + '</span><button type = "button" class= "close dvFiltryClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            }
                        });
                        dvSkupinyFiltru_pridat.splice(indexToRemove - 1, 1);
                        $('.dvFiltryClose').unbind();
                        $('.dvFiltryClose').on('click', dvFiltryCloseClick);
                        return false;
                    }

                    function dvFiltryClick() {
                        console.log(dvSkupinyFiltru);
                        console.log(dvSkupinyFiltru_pridat);
                        jQuery('#NastaveniModal').modal('hide');
                        jQuery('#dvSkupinaFiltruModal').modal({backdrop: 'static', keyboard: false});
                        document.getElementById("dvJednotlivyFiltr").innerHTML = "";
                        dvSkupinyFiltru_pridat = JSON.parse(JSON.stringify(dvSkupinyFiltru));
                        var i = 1;
                        dvSkupinyFiltru.forEach(function () {
                        var filtrString = '<div class="row bg-info dvSkupinaFiltru" poziceFiltru="' + i + '" style="margin: 5px;cursor: pointer;"><span>Skupina filtrů - ' + i + '</span><button type = "button" class= "close dvFiltryClose" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        ++i;
                        document.getElementById("dvFiltr").insertAdjacentHTML('beforeend', filtrString);
                    });
                        $('.dvSkupinaFiltru').unbind();
                        $('.dvSkupinaFiltru').on('click', dvSkupinaFiltruClick);
                        $('.dvFiltrClose').unbind();
                        $('.dvFiltrClose').on('click', dvFiltrCloseClick);
                        $('.dvFiltryClose').unbind();
                        $('.dvFiltryClose').on('click', dvFiltryCloseClick);
                    }

                    function dvSkupinaFiltruClick() {
                    document.getElementById("dvJednotlivyFiltr").innerHTML = "";
                    vybranaSkupina = $(this).attr("poziceFiltru");
                    editovanaSkupina = JSON.parse(JSON.stringify(dvSkupinyFiltru_pridat[vybranaSkupina-1]));
                    console.log("Vybraná skupina: " + vybranaSkupina);
                    jQuery('#dvJednotlivyFiltrModal').modal({backdrop: 'static', keyboard: false});
                    console.log("dvSkupinyFiltru");
                    console.log(dvSkupinyFiltru);
                    console.log("dvSkupinyFiltru_pridat");
                    console.log(dvSkupinyFiltru_pridat);
                    dvSkupinyFiltru_pridat[vybranaSkupina - 1].forEach(function (filtr, index) {
                        console.log(filtr);
                        var str = '<div class="row bg-info dvJednotlivy" poziceFiltru="' + Number(Number(index) + 1) + '" style="margin: 5px;"><span ><select class="dvSelectPole" style="margin-right: 5px;"></select><select class="dvSelectOperator" style="margin-right: 5px;"></select><input type="text" class="dvHodnota"></span><button type = "button" class= "close dvFiltrClose" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        document.getElementById("dvJednotlivyFiltr").insertAdjacentHTML('beforeend', str);

                        $("div[poziceFiltru='" + (index + 1) + "'].dvJednotlivy").children("span").children(".dvSelectPole").each(function () {
                            if ($(this)[0].options.length == 0) {
                                for (var i = 0, l = optionsPole.length; i < l; i++) {
                                    var option = optionsPole[i];
                                    $(this)[0].options.add(new Option(option.text, option.value, option.selected));
                                    $(this).val(filtr['sloupec']);
                                }
                            }
                        });
                        $("div[poziceFiltru='" + (index + 1) + "'].dvJednotlivy").children("span").children(".dvSelectOperator").each(function () {
                            if ($(this)[0].options.length == 0) {
                                for (var i = 0, l = optionsOperators.length; i < l; i++) {
                                    var option = optionsOperators[i];
                                    $(this)[0].options.add(new Option(option.text, option.value, option.selected));
                                    $(this).val(filtr['operace']);
                                }
                            }
                        })
                        $("div[poziceFiltru='" + (index + 1) + "'].dvJednotlivy").children("span").children(".dvHodnota").each(function () {
                            $(this).val(filtr['hodnota']);
                        })
                        //$("div[poziceFiltru='"+index+"']").children().children(".dvSelectPole")

                    })
                    $('.dvFiltrClose').on('click', dvFiltrCloseClick);
                }

                    jQuery('#dtViewFiltry').on('click', dvFiltryClick);
                }



                function createConfig() {
                    console.log("broouuuuuu")
                    jQuery.ajax({
                        async: true,
                        beforeSend: function beforeSend(xhr, settings) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        },
                        type: 'POST',
                        url: dtview_vytvoritCfg,
                        data: {
                            'dv_id': vybrane_zobrazeni,
                            'dt_id': vybrany_table
                        }
                    });
                };
            }
        });
    };
    this.construct(options);
};
