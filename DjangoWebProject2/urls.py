"""
Definition of urls for DjangoWebProject2.
"""

from datetime import datetime
from django.urls import path
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from app import forms, views
from django_dtview import views as dtViews


urlpatterns = [
    path('admin/', admin.site.urls),

    path('testovaciFiltr/', views.ZobrazeniProDtview.as_view(),name='test'),
    path('', views.index, name='index'),



    path('dtviewGetCfg/', dtViews.DtviewView, name='dtviewGetCfg'),
    path('konfigurace/', dtViews.dtviewConfig, name='dtviewConfig'),
    path('konfiguraceView/', dtViews.konfiguraceView, name='konfiguraceView'),
    path('dtviewgetCreateCfg/', dtViews.dtviewCreateCfg, name='dtviewCreateCfg'),
    path('dtviewgetViewCfg/', dtViews.DtviewGetViewCfg.as_view(), name='dtviewGetViewCfg'),
    path('dtviewExportData/', dtViews.DtviewExportData.as_view(), name="dtviewExportData"),
    path('dtviewImportData/', dtViews.DtviewImportData.as_view(), name="dtviewImportData"),
    path('dtviewUlozitView/', dtViews.ulozitView, name="dtviewUlozitView"),
    path('dtviewVytvoritView/', dtViews.vytvoritView, name="dtviewVytvoritView"),
    path('dtviewVymazatView/', dtViews.vymazatView, name="dtviewVymazatView"),
    path('dtviewConfigDt/', dtViews.ConfigDt, name="dtviewConfigDt"),
    path('dtviewConfigDv/', dtViews.ConfigDv, name="dtviewConfigDv"),
    path('dtviewConfigDtcol/', dtViews.ConfigDtcol, name="dtviewConfigDtcol"),
    path('dtviewConfigDvcol/', dtViews.ConfigDvcol, name="dtviewConfigDvcol"),
    path('dtviewDefault/', dtViews.Dtview.as_view(), name="dtviewDefault"),
]
